import Nedb from 'nedb/browser-version/out/nedb'

export const posts = new Nedb({
    filename: 'posts',
    autoload: true
})

export const appendix = new Nedb({
    filename: 'appendix',
    autoload: true
})

/**
 * به روز رسانی فهرست
 * @param { object } appendixObject 
 */
export function updateAppendix(appendixObject) {
    return new Promise((resolve, reject) => {
        appendix.update({
            isAppendix: true
        }, {
            isAppendix: true,
            appendix: appendixObject
        }, {
            upsert: true
        }, (err, count, isUpsert) => {
            if (err) {
                return void reject(err)
            }
            resolve(count)
        })
    })
}

/**
 * گرفتن فهرست
 */
export function getAppendix() {
    return new Promise((resolve, reject) => {
        appendix.findOne({
            isAppendix: true
        }, (err, document) => {
            if (err) {
                return void reject(err);
            }
            resolve(document && document.appendix)
        })
    })
}

/**
 * افزودن یک پست جدید
 * @param { rise.Post } post 
 * @returns { Promise<rise.Post> }
 */
export function addPost(post) {
    return new Promise((resolve, reject) => {
        posts.insert(post, (err, doc) => {
            if (err) {
                return void reject(err)
            }
            resolve(doc)
        })
    })
}

/**
 * ویرایش یک پست
 * @param { rise.Post } post 
 * @returns { Promise<number> }
 */
export function updatePost(post) {
    return new Promise((resolve, reject) => {
        posts.update({_id: post._id}, post, {}, (err, count) => {
            if (err) {
                return void reject(err)
            }
            resolve(count)
        })
    })
}

/**
 * حذف یک پست
 * @param { rise.Post } post
 * @returns { Promise<number> }
 */
export function removePost(post) {
    return new Promise((resolve, reject) => {
        posts.remove({ _id: post._id }, {}, (err, count) => {
            if (err) {
                return void reject(err)
            }
            resolve(count)
        })
    })
}

/**
 * گرفتن پست
 * @param { any } postId
 * @returns { Promise<rise.Post> }
 */
export function getPost(postId) {
    return new Promise((resolve, reject) => {
        posts.findOne({ _id: postId }, (err, doc) => {
            if (err) {
                return void reject(err)
            }
            resolve(doc)
        })
    })
}
