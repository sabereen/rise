import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import store from './store'

// window.jQuery = window.$ = require('jquery')

new Vue({
  router,

  data: {
    isAdminPanel: true
  },

  store,
  render: h => h(App)
}).$mount('#app')
