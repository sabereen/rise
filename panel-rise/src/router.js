import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Editor from './views/Editor.vue'
import Charts from './views/Charts.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'posts',
      component: Editor
    },
    {
      path: '/charts',
      name: 'charts',
      component: Charts
    }
  ]
})
