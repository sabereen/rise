const path = require('path')

module.exports = {
    configureWebpack: {
        resolve: {
            alias: {
                '@app': path.resolve(__dirname, '..', 'app-rise/src'),
                '@panel': path.resolve(__dirname, './src')
            }
        }
    }
}