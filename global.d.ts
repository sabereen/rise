declare namespace rise {
    type ContentType = 'text' | 'image' | 'chart'

    interface Post {
        _id: any;
        title: string;
        contents: Array<Content>;
        author: any;
        template: string;
        // type: PostType;
    }

    interface Content<T> {
        _id: any;
        text: string;
        page: any;
        title: string;
        type: ContentType;
        tags: Array<string>;
        reference: string; // 'link' | 'title';
        content: T
    }

    interface Chart {
        _id: any;
        // post: string;
        data: any;
        options: any;
    }

    interface Media {
        _id: any;
        file: any;
        type: string;
        size: number;
        modifiedDate: Date
    }
}
