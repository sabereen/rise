import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Book from './views/Book.vue'
import Appendix from './views/Appendix.vue'
import Search from './views/Search.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/book',
      name: 'book',
      component: Book
    },
    {
      path: '/appendix',
      name: 'appendix', 
      component: Appendix
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path: '/admin/catalog',
      name: 'admin-catalog',
      component: () => import('./views/CatalogCreator.vue')
    }
  ]
})
