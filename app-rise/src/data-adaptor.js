import { posts, contents, db, charts, medias } from './database'

export async function getPost(id) {
    let post
    await db.transaction('r', posts, contents, async () => {
        post = await posts.get(id)
        post.contents = post.contents.map(async contentId => {
            let content = await contents.get(contentId)
            if (content.type == 'chart') {
                content.data = await charts.get(content.data)
            } else if (content.type == 'image') {
                content.data = await medias.get(content.data)
            }
            return content
        })
    })
    return post
}
