export default {
    state: {
      book: {
        post: {
          children: [
              {
                  id: 'sfsdfsdfsdfs',
                  type: 'text',
                  content: {
                      text: 'متن'
                  }
              }
          ]
      }
      },
      appendix: [
        {
          title: 'فصل اوّل: استقلال',
          color: 'green',
          children: [
            {
              title: 'از وابستگی دوران پهلوی تا استقلال ایران اسلامی',
            }, {
              title: 'استقلال در کشورهای دنیا',
            }
          ]
        }, {
          title: 'فصل دوم: گسترش عدالت اجتماعی',
          children: [
            {
              title: 'نابرابری'
            }, {
              title: 'فقر'
            }
          ]
        }
      ]
    },
    mutations: {
      setBookPost(state, payload) {
        state.book.post = payload
      }
    },
    actions: {}
  }