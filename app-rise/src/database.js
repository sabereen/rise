import Dexie from 'dexie'

export const db = new Dexie('rise')

db.version(1).stores({
    posts: 'id, title, *contents', // author, template, type {'text', 'image', 'chart'}
    contents: 'id, text, page, title, type, *tags', // reference{link,title}, content
    charts: 'id', // post, data, options
    medias: 'id', // file, type, size, modifiedDate
})

db.open()

/** @type{Dexie.Table<{id, title, contents: Array, author, template}>} */
export const posts = db.posts

/** @type{Dexie.Table<{id, text, page, title, type: 'text' | 'image' | 'chart', tags: Array<String>, content, reference: {link, title}}>} */
export const contents = db.contents

/** @type{Dexie.Table<data: {labels: Array, series: Array}, options>} */
export const charts = db.charts

/** @type{Dexie.Table<{file}>} */
export const medias = db.medias
