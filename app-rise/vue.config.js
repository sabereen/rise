const path = require('path')

module.exports = {
  pwa: {
    name: 'صعود چهل ساله'
  },

  configureWebpack: {
    resolve: {
        alias: {
            '@app': path.resolve(__dirname, './src'),
            '@panel': path.resolve(__dirname, '..', 'panel-rise/src')
        }
    }
  },

  lintOnSave: false
}